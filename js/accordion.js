$(function() {
		var Accordion = function(el, multiple) {
				this.el = el || {};
				this.multiple = multiple || false;

				var links = this.el.find('.article-title');
				links.on('click', {
						el: this.el,
						multiple: this.multiple
				}, this.dropdown)
		}

		Accordion.prototype.dropdown = function(e) {
				var $el = e.data.el;
				$this = $(this),
						$next = $this.next();

				$next.slideToggle();
				$this.parent().toggleClass('open');

				//if (!e.data.multiple) {
				//		$el.find('.accordion-content').not($next).slideUp().parent().removeClass('open');
				//};
		}
		
		var accordion = new Accordion($('#accordion'), false);

		$('#side_menu_set').on('click', 'h4 a', function (event) {

			var index = $(this).attr('href');

			$("#tabs li a[href|='"+index+"']").trigger('click');

			//$('#tabs').tabs('option', 'active', 1);

			console.log(index);

		  	return false;

		});
});