<?php get_header(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>

	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_innerpage">
				<div class="l_wrapper">
					<h2 class="c_ttl-b"><?php post_type_archive_title(); ?>一覧</h2>					
					
					<div class="p_column__list">
						<?php custom_posttype('','column','');
						if ($w_query->have_posts()) :
							echo '<ul>';
							while ($w_query->have_posts()) :
								$w_query->the_post();
								$c_title = get_the_title();
								$c_link = get_permalink();
								$c_thumb = get_field('thumbnail_image');
								$c_visual = get_field('main_visual'); ?>

								<li>
									<div class="p_column__thumb">
									<?php 
										if ($c_thumb) {
										echo '<img src="'.esc_url($c_thumb['url']).'" alt="'.$c_title.'">';
										} elseif ($c_visual) {
											echo '<div class="is_visual" style="background-image:url('.esc_url($c_visual['url']).');">&nbsp;</div>';
										} 
									?>
									</div>

									<div class="p_column__content">
										<h3 class="c_ttl-d"><?php echo $c_title; ?></h3>
										<?php 
											$c_exc = preg_replace(" ([.*?])",'',get_the_content());
											$c_exc = strip_shortcodes($c_exc);
											$c_exc = strip_tags($c_exc);
											$c_exc = mb_substr($c_exc, 0, 90);
											$c_exc = trim(preg_replace( '/\s+/', ' ', $c_exc));
											$c_exc = $c_exc.'…';
											echo $c_exc;
										?>
										<div class="p_pickup__btn">
											<a href="<?php echo $c_link; ?>" class="c_btn is_nobd"><span class="c_btn__txt">MORE</span></a>
										</div>
									</div>						            
								</li>
													
							<?php
							endwhile;
							echo '</ul>';

						posts_pagination();							
						endif;
						wp_reset_postdata(); //クエリのリセット ?>						
					</div>

				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>

		</main>
	</div>

<?php get_footer(); ?>
