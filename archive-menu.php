<?php get_header(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>

	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_innerpage">
				<div class="l_wrapper">
					<h2 class="c_ttl-b"><?php post_type_archive_title(); ?> Menu</h2>					
					
					<div class="p_news__list" style="width: 100%;">					
						<?php //custom_posttype('','menu','');
						$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
						$w_query = new WP_Query( 
							array(
									'post_type'     => 'menu',
									'paged'         => $paged,
									'orderby'       => 'date',
									'order'         => 'DESC',	
									'suppress_filters' => true,												
									'post_status'   => 'publish'
							)
						);

						if ($w_query->have_posts()) :
							echo '<ul>';
							while ($w_query->have_posts()) :
								$w_query->the_post();
								$pt_default = get_the_title();
								$permalink = get_permalink(); ?>

								
							
							<?php
							endwhile;
							echo '</ul>';

						posts_pagination();							
						endif;
						wp_reset_postdata(); //クエリのリセット ?>						
					</div>

					<div id="tabs" class="p_menu__content">

						<ul id="face" class="p_menu__tabs">
							<li><a>FACE<span>お顔のお悩み</span></a></li>
						</ul>
						<div class="p_menu__tbl">
							<div id="sagging" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>たるみ・リフトアップ</span></h3>
								<div class="p_menu__inner" >
									<ul class="p_menu__tlist">
										<li><a href="/menu/hifu/">医療HIFU（ハイフ）</a></li>
										<li><a href="/menu/tenor/">テノール</a></li>
										<li><a href="/menu/botox/">ボトックス注射</a></li>
										<li><a href="/menu/threadlift/">スレッドリフト</a></li>
										<li><a href="/menu/photorf/">フォトRF（オーロラ）</a></li>
										<li><a href="/menu/prp/">進化型PRP皮膚再生療法</a></li>
										<li><a href="/menu/hyaluronic/">ヒアルロン酸注入</a></li>
										<li><a href="/menu/minifacelift/">ミニフェイスリフト（部分切開法）</a></li>
									</ul>
								</div>	
							</div>			
						
							<div id="dimple" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>くぼみ・こけ</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/prp/">進化型PRP皮膚再生療法</a></li>
										<li><a href="/menu/hyaluronic/">ヒアルロン酸注入</a></li>
									</ul>
								</div>
							</div>
										
							<div id="doubleeye" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>二重まぶた・目元形成</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/eyes/">二重まぶた・目元（埋没法・切開法など）</a></li>
									</ul>
								</div>		
							</div>
					
							<div id="nose" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>鼻</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/hyaluronic/">ヒアルロン酸注入</a></li>
									</ul>
								</div>	
							</div>
	
							<div id="lips" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>唇・口元</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/hyaluronic/">ヒアルロン酸注入</a></li>
									</ul>
								</div>	
							</div>
				
							<div id="smallface" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>小顔</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/bnls/">BNLS neo</a></li>
										<li><a href="/menu/hifu/">医療HIFU（ハイフ）</a></li>
										<li><a href="/menu/botox/">ボトックス注射</a></li>
										<li><a href="/menu/threadlift/">スレッドリフト</a></li>
									</ul>
								</div>
							</div>
						</div>

						<ul id="skin" class="p_menu__tabs">
							<li><a>SKIN<span>お肌のお悩み</span></a></li>
						</ul>
						<div class="p_menu__tbl">
							<div id="spots" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>シミ・肝斑</span></h3>
								<div class="p_menu__inner" >
									<ul class="p_menu__tlist">
										<li><a href="/menu/photorf/">フォトRF（オーロラ）</a></li>
										<li><a href="/menu/q-yag/">QスイッチYAGレーザー</a></li>
										<li><a href="/menu/electroporation/#">エレクトロポレーション</a></li>
										<li><a href="/menu/medicine/">内服薬</a></li>
										<li><a href="/menu/max-toning/#">肝斑レーザートーニング</a></li>
										<li><a href="/menu/prp/">進化型PRP皮膚再生療法</a></li>
									</ul>
								</div>
							</div>
				
							<div id="wrinkles" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>シワ</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/prp/">進化型PRP皮膚再生療法</a></li>
										<li><a href="/menu/hyaluronic/">ヒアルロン酸注入</a></li>
										<li><a href="/menu/hyaluronic/">ヒアルロン酸注入</a></li>
										<li><a href="/menu/hifu/">医療HIFU（ハイフ）</a></li>
										<li><a href="/menu/tenor/">テノール</a></li>
										<li><a href="/menu/suikou/">水光注射</a></li>
										<li><a href="/menu/peeling/">ケミカルピーリング</a></li>
										<li><a href="/menu/hscm/#">臍帯血再生因子療法</a></li>
										<li><a href="/menu/botox/">ボトックス注射</a></li>
										<li><a href="/menu/photorf/">フォトRF（オーロラ）</a></li>
										<li><a href="/menu/dermapen4/">ダーマペン4</a></li>
										<li><a href="/menu/electroporation/">エレクトロポレーション</a></li>
									</ul>
								</div>
							</div>
			
							<div id="warts" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>イボ・ホクロ</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/co2-laser/">CO2（炭酸ガス）レーザー</a></li>
									</ul>
								</div>	
							</div>
					
							<div id="pore" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>毛穴・黒ずみ</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/fractional/">CO2フラクショナルレーザー</a></li>
										<li><a href="/menu/dermapen4/">ダーマペン4</a></li>
										<li><a href="/menu/electroporation/">エレクトロポレーション</a></li>
										<li><a href="/menu/ion/">イオン導入</a></li>
										<li><a href="/menu/electroporation/">エレクトロポレーション</a></li>
										<li><a href="/menu/suikou/">水光注射</a></li>
										<li><a href="/menu/peeling/">ケミカルピーリング</a></li>
									</ul>
								</div>
							</div>
				
							<div id="whitening" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>美白・くすみ</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/suikou/">水光注射</a></li>
										<li><a href="/menu/peeling/#">ケミカルピーリング</a></li>
										<li><a href="/menu/electroporation/">エレクトロポレーション</a></li>
										<li><a href="/menu/ion/">イオン導入</a></li>
									</ul>
								</div>	
							</div>
				
							<div id="acne" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head "><span>ニキビ・ニキビ跡</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/fractional/">CO2フラクショナルレーザー </a></li>
										<li><a href="/menu/suikou/">水光注射</a></li>
										<li><a href="/menu/peeling/">ケミカルピーリング</a></li>
										<li><a href="/menu/dermapen4/">ダーマペン4</a></li>
										<li><a href="/menu/electroporation/">エレクトロポレーション</a></li>
										<li><a href="/menu/ion/">イオン導入</a></li>
									</ul>
								</div>
							</div>
						</div>

						<ul id="body" class="p_menu__tabs">
							<li><a>BODY<span>身体のお悩み</span></a></li>
						</ul>
						<div class="p_menu__tbl">
							<div id="datsumou" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>医療脱毛</span></h3>
								<div class="p_menu__inner" >
									<ul class="p_menu__tlist">
										<li><a href="/menu/datsumou/">医療レーザー脱毛（ベラックス）</a></li>
									</ul>
								</div>
							</div>
			
							<div id="armpit" class="p_menu__tbl__row">	
								<h3 class="c_ttl-c p_menu__head"><span>ワキが・多汗症</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/botox/">ボトックス注射</a></li>
									</ul>
								</div>
							</div>
					
							<div class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>イボ・ホクロ</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/co2-laser/">CO2（炭酸ガス）レーザー</a></li>
									</ul>
								</div>
							</div>

							<div id="slimming" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>部分痩せ・痩身</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/bnls/">BNLS neo</a></li>
									</ul>
								</div>
							</div>
				
							<div id="warts" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>蒙古性苔癬</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/fractional/">CO2フラクショナルレーザー</a></li>
										<li><a href="/menu/dermapen4/">ダーマペン4</a></li>
									</ul>
								</div>
							</div>
						</div>
							
						<ul id="other" class="p_menu__tabs">
							<li><a>OTHER<span>その他</span></a></li>
						</ul>
						<div class="p_menu__tbl">
							<div id="injection" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>注射・点滴</span></h3>
								<div class="p_menu__inner" >
									<ul class="p_menu__tlist">
										<li><a href="/menu/placenta/">美肌・美白注射<br>（プラセンタ/グルタチオン/ビタミンC）</a></li>
										<li><a href="/menu/vitamin/">高濃度ビタミンC点滴</a></li>
									</ul>
								</div>	
							</div>
					
							<div id="hair" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>毛髪再生</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/hscm/">臍帯血再生因子療法</a></li>
									</ul>
								</div>
							</div>
			
							<div class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>イボ・ホクロ</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/co2-laser/">CO2（炭酸ガス）レーザー</a></li>
									</ul>
								</div>	
							</div>
				
							<div id="artmake" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>医療アートメイク</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/artmake/">医療アートメイク</a></li>
									</ul>
								</div>
							</div>
				
							<div id="pierce" class="p_menu__tbl__row">
								<h3 class="c_ttl-c p_menu__head"><span>ピアス</span></h3>
								<div class="p_menu__inner">
									<ul class="p_menu__tlist">
										<li><a href="/menu/pierce/">医療ピアス</a></li>
									</ul>
								</div>
							</div>
						</div>

					</div><!-- .p_troubles__content -->			

				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>

		</main>
	</div>

<?php get_footer(); ?>
