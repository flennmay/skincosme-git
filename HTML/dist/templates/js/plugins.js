$(document).ready(function(){

	// Troubles Tabs
	$( "#tabs" ).tabs();

	// Header Fixed Position
	$('.c_btn-fixed').css({
		'padding-top' : $('.p_header ').height() + 30
	});
	$(window).scroll(function() {
		var scroll = $(this).scrollTop();
		var scrollH = $('.f_site__header ').height() + $('.f_site_banner ').height();
		var scrollhead = $('.p_header ').height();

		if ((scroll > 300) && (scroll < scrollH)) {
			$('header.p_header').addClass('off');			
		} else {
			$('header.p_header').removeClass('off');
		}
		if (scroll > scrollH) {			
			$('header.p_header').addClass('affix');
		} else {
			$('header.p_header').removeClass('affix');
		}
		
		if (scroll > scrollhead) {
			$('.c_btn-fixed a.is_pagetop img').fadeIn();
		} else {
			$('.c_btn-fixed a.is_pagetop img').fadeOut();
		}	
	});

	$(".c_btn-fixed a.is_pagetop").click(function() {
		$("html, body").animate({scrollTop: 0}, 1000);
	});

	//Main Slider
	$('.fade').slick({
		dots: true,
		infinite: true,
		speed: 300,
		autoplay: true,
		fade: true,
		arrows: false,
		pauseOnHover: false,
		respondTo: 'window',
		responsive: null,
		rows: 1,
		slidesToShow: 1,
		swipe: true,
		cssEase: 'linear'
	});

	// Pick-Up Slick
	$('.p_pickup__slick').slick({
		dots: false,
		arrows: true,
		infinite: true,
		speed: 1000,
		slidesToShow: 1,
		slidesToScroll: 3,
		autoplay: false,
		autoplaySpeed: 0,
		cssEase: 'ease-in-out',
		variableWidth: true,
		centerMode: true,
		mobileFirst: true,
		responsive: [
			{
				breakpoint: 991,
				settings: {
				slidesToShow: 1,
				slidesToScroll: 1
				}
			}
		]
	});
	

	// Swiper effect
	var mySwiper = new Swiper ('.swiper-container', {
		autoplay: {
			delay: 5000,
		  },
		disableOnInteraction: true,
		loop: true,
		// If we need pagination
		pagination: {
			el: '.swiper-pagination',
			type: 'bullets',
			clickable: true,
		  },

		// Navigation arrows
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});

});


$(window).on('resize',function(){
	

});

// Header Menu Dropdown
$(function() {
	$('.p_header__nav__list ul.menu > li').hover(function() {
		$(this).children('.p_dropdown').slideToggle();
	}, function() {
		$(this).children('.p_dropdown').hide();
	});
});

// SP Menu Nav
$(function(){
	$('.navSpOpen').click(function(){
		$(this).toggleClass('active');
		$('.navSpClose').toggleClass('active');
		$('nav').toggleClass('open');
	});

	$('.navSpClose').click(function(){
		$(this).toggleClass('active');
		$('.navSpOpen').toggleClass('active');
		$('nav').toggleClass('open');
	});
});

// Main Navigation Current Setting
$(function() {
	$('.p_header__nav__list ul.menu li a').each(function(){
		var $href = $(this).attr('href');
		if(location.href.match($href)) {
				$(this).parent().addClass('current');
		} else {
				$(this).parent().removeClass('current');
		}
	});
});

// Smooth Scroll
$(function() {
	var scroll = new SmoothScroll('.js_scroll', {
		speed: 500,//スクロールする速さ
		header: '.p_header'//固定ヘッダーがある場合
	});
});