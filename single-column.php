<?php 

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>
	
	<div id="main_area" class="f_site_main">
		<main>
			<section class="f_section is_white">
				<div class="l_wrapper">
					<h3 class="c_ttl-c"><span><?php the_title(); ?></span></h3>										
					<?php 
					$c_visual = get_field('main_visual');
					if ($c_visual) { ?>
					<div class="p_column__visual">
						<img src="<?php echo esc_url($c_visual['url']); ?>" alt="<?php the_title(); ?>">
					</div>
					<?php } ?>
					<div class="p_column is_bglb">
						<?php the_content(); ?>
					</div>
					<div class="c_postnav">
						<div class="is_pnav-prev"><?php previous_post_link( '%link','<' ) ?></div>
						<div class="is_pnav-next"><?php next_post_link( '%link','>' ); ?></div>
					</div>
				</div>
			</section>
			<?php get_template_part( 'inc/contact' ); ?>
		</main>
	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
