<?php 

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'inc/inner_banner' ); ?>
	<?php get_template_part( 'inc/breadcrumb' ); ?>
	
	<div id="main_area" class="f_site_main">
		<main>			

			<section class="f_innerpage">
				<div class="l_wrapper">
					<h2 class="c_ttl-b"><?php the_title(); ?></h2>
					
					<div class="c_ttl-d no_bd"><?php the_time('Y.m.d'); ?></div>
					<?php the_content(); ?>
					<div class="mt80 c_txt-c ">
						<a class="c_btn" href="<?php echo get_home_url(); ?>/news"><span class="c_btn__txt">お知らせ一覧に戻る</span></a>
					</div>
				</div>
			</section>

			<?php get_template_part( 'inc/contact' ); ?>
		</main>
	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
